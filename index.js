// console.log("Hello World");

// [SECTION] Objects
// An object is a data type that is used to represent a real world object.
// It is a collection of related data and/or functionalities.
// Information is stored in object represented in "key:value" pair
		// key -> property of the object
		// value -> actual data to be stored
// Different data type may be stored in an object's property creating a complex data structures.

// Two ways in creating object in javascript
	// 1. Object Literal Notation
	// 2. Object Constructor Notation

	// Object Literal Notation
	// Creating objects using initializers/literal notation.
	/*
		- Syntax:
			let objectName = {
				keyA: valueA,
				keyB: valueB
			}

	*/

	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999
	};

	console.log("Result from creating objects using initializers/literal notation:");
	console.log(cellphone);
	console.log(typeof cellphone);

	let cellphone2 = {
		name: "Iphone 13",
		manufactureDate: 2021
	};

	console.log(cellphone2);

	let cellphone3 = {
		name: "Xiaomi 11t Pro",
		manufactureDate: "2021"
	};

	console.log(cellphone3);


	// Object Constructor Notation
	// Creating objects using a constructor function.
		// Creates a reusable function to create several objects that have the same data structure. (acts as blueprint)

	/*
		- Syntax:
			function objectName(keyA, keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			}
			
			// 'this' keyword refers to the properties within the object.
				// It allows the assignment of new object's properties by associating them with values received from the constructor function's parameter.
	
	*/

	function Laptop(name, manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}

	// Create an instance object using the Laptop constructor
	let laptop = new Laptop("Lenovo", 2008);
	console.log("Result from creating objects using object constructor:");
	console.log(laptop);
	// The 'new' operator that creates instance of an object. (new object)

	let myLaptop = new Laptop("Macbook Air", 2020);
	console.log("Result from creating objects using object constructor:");
	console.log(myLaptop);

	// Returns 'undefined' without the 'new' operator because the Laptop function does not have a return statement.
	let oldLaptop = Laptop ("Portal R2E CCMC", 1980);
	console.log("Result from creating objects using object constructor:");
	console.log(oldLaptop);


	// Creating of empty objects
	let computer = {} //object literal notation
	let myComputer = new Object(); // object constructor notation -> instantiate.



// [SECTION] Accessing Objects Properties

// Using dot notation
console.log("Result from dot notation: " +myLaptop.name);

// Using square bracket notation
console.log("Result from square bracket notation: " +myLaptop["name"]);

// When accessing array of objects
// Accessing object properties using the square bracket notation and array indexes can cause confusion.

let arrayObj = [laptop, myLaptop];
console.log(arrayObj[0]); //to acces the first element of the array within the object
console.log(arrayObj[0].name); //this tells us that array[0] is an object by using the dot notation


// [SECTION] Initializing/Adding/Deleting/Reassigning Object Properties

	let car = {};
	console.log("Current value of car object:");
	console.log(car);

	// Initializing/adding object properties.
	car.name = "Honda Civic";
	console.log("Result from adding properties using dot notation:")
	console.log(car);

	// Initialize/adding object properties using square bracket notation (not recommended)
	console.log("Result from adding properties using square bracket notation:")
	car["manufacture date"] = 2019;
	console.log(car);

	
	// Deleting object properties
	delete car["manufacture date"];
	console.log("Result from deleting properties");
	console.log(car);

	car["manufactureDate"] = 2019;
	console.log(car);


	// Reassigning object property values
	car.name = "Toyota Vios";
	console.log("Result from reassigning property values:");
	console.log(car);



// [SECTION] Object Methods
	// A method is function which is a property of an object.

	let person = {
		name: "John",
		talk: function(){
			console.log("Hello my name is " +this.name);
		}
	}
	console.log(person);
	console.log("Result from object method:");
	person.talk();

	person.walk = function(steps){
		console.log(this.name + " walked " +steps+ " steps forward");
	}
	console.log(person);
	person.walk(50);


	// Methods are useful from creating reusable functions that perform tasks related to objects
	let friend = {
		firstName: "Joe",
		lastName: "Smith",
		address: {
			city: "Austin",
			country: "Texas",
		},
		emails: ["joe@mail.com", "joesmith@mail.xyz"],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName + ". I live in " + this.address.city + " " + this.address.country + ".")
		}
	}

	friend.introduce();


// [SECTION] Real World Application of Objects
/*
	Scenario:
	1. We would like to create a game that would have a several pokemon interact with each other/
	2. Every pokemon would have the same set of stats, properties and function.

	Stats:
	name
	level
	health = level * 2
	attack = level

*/

	// Create an object constructor to lessen the process in creating the pokemon.

	function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level;

		// Method
							//target parameter represents another pokemon object.
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name)
			// Reduces the target object's health property by subtractingn and reassigning it's value based on the pokemon's attack
			// target.health = target.health - this.attack
			target.health -= this.attack
		
			// if target health less than or equal to 0 we will invoke the faint() method, otherwise printout the pokemon's new health:
			// example if health is not less than 0
				// rattat's health is now reduced to 5

			console.log(target.name + "'s health is now reduced to " + target.health)

			if (target.health <= 0){
				// Invoke the faint method from the target object if the health is less than or equal to 0
				target.faint()
				;
			}

			// else {
			// 	// console.log(target.name + "'s health is now reduced to " + target.health);

			// 	// if target health is less than or equal to 0
				
			// }
			
		}
		this.faint = function(){
			console.log(this.name + " fainted.");

		}
	}
	
	let pikachu = new Pokemon("Pikachu", 99);
	console.log(pikachu);

	let rattata = new Pokemon("Rattata", 5);
	console.log(rattata);

	pikachu.tackle(rattata);